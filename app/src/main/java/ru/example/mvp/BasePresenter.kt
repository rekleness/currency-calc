package ru.example.mvp

abstract class BasePresenter<V : MvpView> : MvpPresenter<V> {

    protected var view: V? = null

    override fun attachView(view: V) {
        this.view = view
    }

    override fun detachView() {
        view = null
    }
}