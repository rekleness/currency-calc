package ru.example.mvp.presenter

import ru.example.App
import ru.example.api.ExchangeRateApi
import ru.example.database.dao.ExchangeRateDao
import ru.example.database.entity.ExchangeRate
import ru.example.enumeraton.Currency
import ru.example.mvp.BasePresenter
import ru.example.mvp.contract.AppContract
import ru.example.util.AsyncTask
import java.math.BigDecimal

class AppPresenter : BasePresenter<AppContract.View>(), AppContract.Presenter {

    private val exchangeRateDao: ExchangeRateDao = App.database.exchangeRateDao()

    private var calcTask: AsyncTask.Worker? = null

    override fun calculateLatest(currencyFrom: Currency, currencyTo: Currency, sumValue: String) {
        calcTask?.interrupt()
        var textSum = ""
        var textCourse = ""
        val convertCurrency = { if (sumValue.isNotBlank()) {
            exchangeRateDao.getRate(currencyFrom, currencyTo)?.let {
                textSum = (BigDecimal(sumValue) * BigDecimal.valueOf(it.value)).toString()
                textCourse = it.value.toString()
            }
        } }
        val latestCall = ExchangeRateApi.latest(currencyFrom, *Currency.values())
        calcTask = AsyncTask.doWork(latestCall) {
            val response = latestCall.execute()
            val body = response.body()
            if (response.isSuccessful && body?.success == true) {
                val rateList = body.rates.map {
                    val rate = ExchangeRate()
                    rate.fromCurrency = currencyFrom
                    rate.toCurrency = it.key
                    rate.value = it.value
                    rate
                }.toList()
                if (rateList.isNotEmpty()) {
                    exchangeRateDao.deleteByFromCurrency(currencyFrom)
                    exchangeRateDao.insertAll(rateList)
                }
                convertCurrency()
            }
        }.workFailed(convertCurrency).postWork {
            view?.setCalculatedSum(textSum)
            view?.setCurrentCourse(textCourse)
        }
        calcTask?.exec()
    }
}