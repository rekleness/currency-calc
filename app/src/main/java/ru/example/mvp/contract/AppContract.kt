package ru.example.mvp.contract

import ru.example.enumeraton.Currency
import ru.example.mvp.MvpPresenter
import ru.example.mvp.MvpView

interface AppContract {

    interface View : MvpView {
        fun setCalculatedSum(sum: String)
        fun setCurrentCourse(course: String)
    }

    interface Presenter : MvpPresenter<View> {
        fun calculateLatest(currencyFrom: Currency, currencyTo: Currency, sumValue: String)
    }
}