package ru.example

import android.app.Application
import androidx.room.Room
import ru.example.database.AppDatabase

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        database = Room.databaseBuilder(applicationContext, AppDatabase::class.java, AppDatabase.NAME).build()
    }

    companion object {

        lateinit var instance: App
            private set

        lateinit var database: AppDatabase
            private set
    }
}