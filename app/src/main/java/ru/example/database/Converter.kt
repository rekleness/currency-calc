package ru.example.database

import androidx.room.TypeConverter
import ru.example.enumeraton.Currency

class Converter {

    @TypeConverter
    fun toCurrency(value: String) = enumValueOf<Currency>(value)

    @TypeConverter
    fun fromCurrency(value: Currency) = value.name
}