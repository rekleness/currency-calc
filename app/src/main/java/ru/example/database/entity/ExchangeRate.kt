package ru.example.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import ru.example.enumeraton.Currency

@Entity(tableName = "exchange_rate")
class ExchangeRate {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long? = null

    @ColumnInfo(name = "from_currency")
    var fromCurrency: Currency? = null

    @ColumnInfo(name = "to_currency")
    var toCurrency: Currency? = null

    @ColumnInfo(name = "value")
    var value: Double = .0
}