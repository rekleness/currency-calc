package ru.example.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ru.example.database.dao.ExchangeRateDao
import ru.example.database.entity.ExchangeRate

@Database(
    version = 1,
    entities = [
        ExchangeRate::class
   ]
)
@TypeConverters(Converter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun exchangeRateDao(): ExchangeRateDao

    companion object {
        const val NAME = "example"
    }
}