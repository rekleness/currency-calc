package ru.example.database.dao

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Update

interface BaseDao<T> {

    @Insert
    fun insert(obj: T)

    @Insert
    fun insertAll(objList: List<T>)

    @Update
    fun update(obj: T)

    @Update
    fun updateAll(objList: List<T>)

    @Delete
    fun delete(obj: T)

    @Delete
    fun deleteAll(objList: List<T>)
}