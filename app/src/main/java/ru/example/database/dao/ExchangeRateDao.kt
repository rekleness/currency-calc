package ru.example.database.dao

import androidx.room.Dao
import androidx.room.Query
import ru.example.database.entity.ExchangeRate
import ru.example.enumeraton.Currency

@Dao
abstract class ExchangeRateDao : BaseDao<ExchangeRate> {

    @Query("SELECT * FROM exchange_rate WHERE from_currency = :fromCurrency AND to_currency = :toCurrency")
    abstract fun getRate(fromCurrency: Currency, toCurrency: Currency): ExchangeRate?

    @Query("SELECT * FROM exchange_rate WHERE from_currency = :fromCurrency")
    abstract fun gelAllByFromCurrency(fromCurrency: Currency): List<ExchangeRate>

    @Query("DELETE FROM exchange_rate WHERE from_currency = :fromCurrency")
    abstract fun deleteByFromCurrency(fromCurrency: Currency): Int
}