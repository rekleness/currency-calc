package ru.example.ui

import android.os.Bundle
import android.text.Editable
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import ru.example.constant.BaseConstant.INIT_CURRENCY_FROM
import ru.example.constant.BaseConstant.INIT_CURRENCY_TO
import ru.example.databinding.ActivityAppBinding
import ru.example.enumeraton.Currency
import ru.example.mvp.contract.AppContract
import ru.example.mvp.presenter.AppPresenter
import ru.example.util.SharedPrefUtil.getFromSharedPref
import ru.example.util.SharedPrefUtil.saveToSharedPref

class AppActivity : AppCompatActivity(), AppContract.View {

    private lateinit var binding: ActivityAppBinding
    private var presenter: AppContract.Presenter? = null

    private lateinit var sumLayout: LinearLayout
    private lateinit var sumLabel: TextView
    private lateinit var sumText: TextView

    private lateinit var courseLayout: LinearLayout
    private lateinit var courseLabel: TextView
    private lateinit var courseText: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = AppPresenter()
        presenter?.attachView(this)

        binding = ActivityAppBinding.inflate(layoutInflater)
        sumLayout = binding.sumLayout
        sumLabel = binding.sumLabel
        sumText = binding.sumText
        courseLayout = binding.courseLayout
        courseLabel = binding.courseLabel
        courseText = binding.courseText

        val currencyItemList = Currency.values().toList()

        val currencyFrom: Spinner = binding.currencyFrom
        val currencyFromAdapter = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item,
            currencyItemList
        )
        currencyFrom.adapter = currencyFromAdapter
        if (savedInstanceState == null) {
            val pref = getFromSharedPref(INIT_CURRENCY_FROM)
            if (pref.isNotEmpty()) currencyFrom.setSelection(enumValueOf<Currency>(pref).ordinal)
        }

        val currencyTo: Spinner = binding.currencyTo
        val currencyToAdapter = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item,
            currencyItemList
        )
        currencyTo.adapter = currencyToAdapter
        if (savedInstanceState == null) {
            val pref = getFromSharedPref(INIT_CURRENCY_TO)
            if (pref.isNotEmpty()) currencyTo.setSelection(enumValueOf<Currency>(pref).ordinal)
        }

        val editValue: EditText = binding.editValue
        editValue.addTextChangedListener(afterTextChanged = { text: Editable? ->
            val textVal = text?.toString()
            if (textVal.isNullOrBlank() || textVal == ".") {
                text?.clear()
                setCalculatedSum("")
                setCurrentCourse("")
            }
        })
        editValue.setOnEditorActionListener { view, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                val imm = applicationContext.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
                view.clearFocus()
                presenter?.calculateLatest(
                    currencyItemList[currencyFrom.selectedItemPosition],
                    currencyItemList[currencyTo.selectedItemPosition],
                    editValue.text.toString()
                )
            }
            false
        }

        class SpinnerSelectListener(private val key: String) : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                saveToSharedPref(key, currencyItemList[position].name)
                presenter?.calculateLatest(
                    currencyItemList[currencyFrom.selectedItemPosition],
                    currencyItemList[currencyTo.selectedItemPosition],
                    editValue.text.toString()
                )
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        currencyFrom.onItemSelectedListener = SpinnerSelectListener(INIT_CURRENCY_FROM)
        currencyTo.onItemSelectedListener = SpinnerSelectListener(INIT_CURRENCY_TO)

        setContentView(binding.root)
    }

    override fun setCalculatedSum(sum: String) {
        if (sum.isBlank()) {
            sumLayout.visibility = View.GONE
        } else {
            sumLayout.visibility = View.VISIBLE
            sumText.text = sum
        }
    }

    override fun setCurrentCourse(course: String) {
        if (course.isBlank()) {
            courseLayout.visibility = View.GONE
        } else {
            courseLayout.visibility = View.VISIBLE
            courseText.text = course
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.detachView()
        presenter = null
    }
}