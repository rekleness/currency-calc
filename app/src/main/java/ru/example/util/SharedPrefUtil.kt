package ru.example.util

import android.content.Context
import android.content.Context.MODE_PRIVATE
import ru.example.App

object SharedPrefUtil {

    private const val APP_PREFERENCE = "APP_PREFERENCE"

    fun saveToSharedPref(key: String, value: String, context: Context = App.instance) {
        val sp = context.getSharedPreferences(APP_PREFERENCE, MODE_PRIVATE)
        val spe = sp.edit()
        spe.putString(key, value)
        spe.apply()
    }

    fun getFromSharedPref(key: String, context: Context = App.instance): String {
        val sp = context.getSharedPreferences(APP_PREFERENCE, MODE_PRIVATE)
        return sp.getString(key, "") ?: ""
    }
}