package ru.example.util

import android.os.Handler
import android.os.Looper
import android.util.Log
import retrofit2.Call
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

object AsyncTask {

    private const val TAG = "AsyncTask"

    class Worker (
        private val executor: ExecutorService,
        private val handler: Handler,
        private val call: Call<*>?,
        private val doWork: () -> Unit
    ) {

        private var isInterrupt = false
        private var workFailed: (() -> Unit)? = null
        private var postWork: (() -> Unit)? = null

        fun workFailed(workFailed: () -> Unit): Worker {
            this.workFailed = workFailed
            return this
        }

        fun postWork(postWork: () -> Unit): Worker {
            this.postWork = postWork
            return this
        }

        fun interrupt() {
            call?.cancel()
            isInterrupt = true
        }

        fun exec() = executor.execute {
            try { doWork() } catch (e: Exception) {
                Log.e(TAG, "doWork() failed", e)
                try {
                    if (isInterrupt) return@execute
                    workFailed?.invoke()
                } catch (e: Exception) { Log.e(TAG, "workFailed() failed", e) }
            }
            if (isInterrupt) return@execute
            handler.post { postWork?.invoke() }
        }
    }

    fun doWork(call: Call<*>? = null, doWork: () -> Unit) = Worker(
        Executors.newSingleThreadExecutor(),
        Handler(Looper.getMainLooper()),
        call,
        doWork
    )
}