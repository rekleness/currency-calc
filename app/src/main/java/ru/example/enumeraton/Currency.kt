package ru.example.enumeraton

enum class Currency(private val text: String) {

    RUB("Рубль"),
    USD("Доллар США"),
    EUR("Евро"),
    GBP("Британский фунт"),
    CHF("Швейцарский франк"),
    CNY("Китайский юань");

    override fun toString() = "${this.name} ${this.text}"
}