package ru.example.api.service

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import ru.example.api.response.exchangerate.LatestRateResp

interface ExchangeRateService {

    @GET("latest")
    fun latest(
        @Query("cbase") cbase: String,
        @Query("symbols") symbols: String
    ): Call<LatestRateResp>
}