package ru.example.api.response.exchangerate

import com.google.gson.annotations.SerializedName
import ru.example.enumeraton.Currency

class LatestRateResp : BaseResp() {
    @SerializedName("base") var base: Currency? = null
    @SerializedName("rates") var rates = emptyMap<Currency, Double>()
}