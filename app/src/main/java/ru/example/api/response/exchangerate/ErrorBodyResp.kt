package ru.example.api.response.exchangerate

import com.google.gson.annotations.SerializedName

class ErrorBodyResp {

    @SerializedName("success") var success: Boolean = false
    @SerializedName("error") var error: Error? = null

    class Error {
        @SerializedName("code") var code: Int = 0
        @SerializedName("info") var info: String = ""
    }
}