package ru.example.api.response.exchangerate

import com.google.gson.annotations.SerializedName

open class BaseResp {
    @SerializedName("success") var success = false
}