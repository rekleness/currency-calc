package ru.example.api

import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


object ApiFactory {

    private const val CONNECTION_TIMEOUT = 5L

    fun instance(url: String, apiKeyParam: String, apiKeyValue: String): Retrofit {
        val client = OkHttpClient.Builder()
            .retryOnConnectionFailure(false)
            .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
            .addInterceptor(QueryInterceptor(apiKeyParam, apiKeyValue))
            .build()
        val gson = GsonBuilder().create()
        return Retrofit.Builder()
            .baseUrl(url)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    private class QueryInterceptor(val apiKeyParam: String, val apiKeyValue: String) : Interceptor {
        override fun intercept(chain: Interceptor.Chain): Response {
            val request = chain.request()
            val newHttpUrl = request.url().newBuilder()
                .addQueryParameter(apiKeyParam, apiKeyValue).build()
            return chain.proceed(request.newBuilder().url(newHttpUrl).build())
        }
    }
}