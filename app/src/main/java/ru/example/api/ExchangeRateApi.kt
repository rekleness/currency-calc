package ru.example.api

import ru.example.api.service.ExchangeRateService
import ru.example.enumeraton.Currency

object ExchangeRateApi {

    private const val URL = "http://data.fixer.io/api/"
    private const val API_KEY_PARAM = "access_key"
    private const val API_KEY_VALUE = "35a3ad0f2f253d37131b68cd1b5953fc"
    private val service = ApiFactory.instance(URL, API_KEY_PARAM, API_KEY_VALUE).create(ExchangeRateService::class.java)

    fun latest(base: Currency, vararg relations: Currency) =
        service.latest(base.name, relations.joinToString(separator = ",", transform = { it.name }))
}